package pl.xayan.mpr7.chart;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class SerieBuilderTest {
    @Test
    public void testBuilder() {
        Point point = new Point();
        point.x = 10;
        point.y = 10;
        SerieBuilder serieBuilder = new SerieBuilder();
        ChartSerie serie = serieBuilder.addPoint(point)
                .addLabel("test")
                .setType(SerieType.Bar)
                .build();

        assertThat(serie.label).isEqualToIgnoringCase("test");
        assertThat(serie.points.size()).isEqualTo(1);
        assertThat(serie.points.get(0)).isEqualToComparingFieldByField(point);
        assertThat(serie.points.get(0).x).isEqualTo(10);
        assertThat(serie.points.get(0).y).isEqualTo(10);
    }
}
