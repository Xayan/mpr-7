package pl.xayan.mpr7.chart;

import java.util.ArrayList;
import java.util.List;

public class SerieBuilder {
    private List<Point> points = new ArrayList<Point>();
    private String label;
    private SerieType serieType;

    public SerieBuilder addPoint(Point point) {
        this.points.add(point);
        return this;
    }

    public SerieBuilder addLabel(String label) {
        this.label = label;
        return this;
    }

    public SerieBuilder withPoints(List<Point> points) {
        this.points = points;
        return this;
    }

    public SerieBuilder setType(SerieType type) {
        this.serieType = type;
        return this;
    }

    public ChartSerie build() {
        ChartSerie serie = new ChartSerie();
        serie.label = label;
        serie.points = points;
        serie.serieType = serieType;
        return serie;
    }
}
