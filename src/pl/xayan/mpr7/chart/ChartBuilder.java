package pl.xayan.mpr7.chart;

import java.util.ArrayList;
import java.util.List;

public class ChartBuilder {
    private List<ChartSerie> series = new ArrayList<>();
    private String title;
    private String subtitle;
    private boolean haveLegend = false;
    private ChartType chartType;

    public ChartBuilder addSerie(ChartSerie serie) {
        series.add(serie);
        return this;
    }

    public ChartBuilder withSeries(List<ChartSerie> series) {
        this.series = series;
        return this;
    }

    public ChartBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public ChartBuilder withSubtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    public ChartBuilder withLegend() {
        this.haveLegend = true;
        return this;
    }

    public ChartBuilder withType(ChartType chartType) {
        this.chartType = chartType;
        return this;
    }
    
    public ChartSettings build() {
        ChartSettings chartSettings = new ChartSettings();
        chartSettings.series = series;
        chartSettings.title = title;
        chartSettings.subtitle = subtitle;
        chartSettings.haveLegend = haveLegend;
        chartSettings.chartType = chartType;
        return chartSettings;
    }
}
